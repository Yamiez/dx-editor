#include "Editor.h"
#include "../Utils/Exporter.h"

Editor::Editor()
	: window_( nullptr ), painter_( nullptr ), current_( nullptr ), 
	  type_( 0 ), control_( nullptr ), events_( nullptr ), 
	  fields_( nullptr ), eventsComponents_( ), fieldsComponents_( ),
	  added_( ), unsaved_( false ), screen_{ static_cast<float>( GetSystemMetrics( SM_CXSCREEN ) ), static_cast<float>( GetSystemMetrics( SM_CYSCREEN ) ) },
	  uiid_( 0 ), addButton_( nullptr ), addCheckbox_( nullptr ),
	  addRichLabel_( nullptr ), addSlider_( nullptr ), addTab_( nullptr ),
	  addTabControl_( nullptr ), addTextbox_( nullptr ), datas_( nullptr )
{
}

Editor::~Editor()
{
	if ( window_ )
		delete window_;

	if ( painter_ )
		delete painter_;

	if ( current_ )
		delete current_;

	if ( control_ )
		delete control_;

	if ( events_ )
		delete events_;

	if ( fields_ )
		delete fields_;

	for ( auto &x : eventsComponents_ )
		delete x;
	eventsComponents_.clear( );

	for ( auto &x : fieldsComponents_ )
		delete x;
	fieldsComponents_.clear( );

	added_.clear( );
}

void Editor::InitializeComponent()
{
	// Window
	window_ = dx::Window::Create( "DXEditor 0.1", "DX Editor | *.dxproj", { { 0, 0 }, screen_ } );
	window_->OnPaint( ) += BIND_METHOD_2( &Editor::Paint, this );
	window_->OnKeyDown( ) += BIND_METHOD_2( &Editor::KeyDown, this );
	window_->OnKeyUp( ) += BIND_METHOD_2( &Editor::KeyUp, this );
	window_->OnKeyDownChar( ) += BIND_METHOD_2( &Editor::KeyDownChar, this );
	window_->OnMouseMoved( ) += BIND_METHOD_2( &Editor::MouseMoved, this );
	window_->OnMouseClicked( ) += BIND_METHOD_2( &Editor::MouseClicked, this );
	window_->OnMouseReleased( ) += BIND_METHOD_2( &Editor::MouseReleased, this );
	window_->OnWindowResize( ) += BIND_METHOD_2( &Editor::WindowResize, this );
	window_->OnWindowClosing( ) += [this]( dx::Window *sender, dx::WindowClosingArgs &args )
	{
		if ( unsaved_ )
		{
			auto res = dx::MsgBox( "You have unsaved changes.\nAre you sure you want to exit without saving.\nThis will discard all unsaved changes.\n'Yes' to exit and discard, 'No' to cancel.", "Unsaved Changes", dx::IconQuestion | dx::YesNo ).Show( );

			if ( res == dx::MsgBox::No )
				args.ShouldClose = false;
		}
	};
	window_->OnWindowClosed( ) += []( dx::Window *Sender )
	{
		dx::Application::get( )->exit( );
	};
	
	window_->Show( );
	window_->SpecializePaint( dx::Window::OnEvent_t );
	window_->Maximize( );

	// Painter
	painter_ = reinterpret_cast<dx::BasePainter*>( dx::Painter::Create( window_ ) );

	// Default font
	auto context = painter_->defaultFont( )->context( );
	context.Quality = PROOF_QUALITY; // we want better quality
	painter_->setDefaultFont( dx::Font::Create( "Consolas", context, painter_ ) ); 


	// ===================== COMPONENTS =====================
	control_ = new dx::TabControl( );
	control_->setUIID( ++uiid_ );
	control_->setLocalPosition( { screen_.x - 300, screen_.y / 2 } );
	control_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	
	// Fields tab
	fields_ = new dx::Tab( );
	fields_->setText( "Fields" );
	fields_->setUIID( ++uiid_ );
	fields_->setSize( { 125, 30 } );
	fields_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );

	// Events tab
	events_ = new dx::Tab( );
	events_->setText( "Events" );
	events_->setUIID( ++uiid_ );
	events_->setSize( { 125, 30 } );
	events_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );


	// First row
	// addButton_
	addButton_ = new dx::Button( );
	addButton_->setText( "Button" );
	addButton_->setUIID( ++uiid_ );
	addButton_->setLocalRegion( { { screen_.x - 300, 150 }, { 100, 30 } } );
	addButton_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addButton_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_button, this );
	
	// addCheckbox_
	addCheckbox_ = new dx::Button( );
	addCheckbox_->setText( "Checkbox" );
	addCheckbox_->setUIID( ++uiid_ );
	addCheckbox_->setSize( { 100, 30 } );
	addCheckbox_->setBottomOf( addButton_ );
	addCheckbox_->setAlignedOf( addButton_ );
	addCheckbox_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addCheckbox_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_checkbox, this );

	// Second row
	// addRichLabel_
	addRichLabel_ = new dx::Button( );
	addRichLabel_->setText( "RichLabel" );
	addRichLabel_->setUIID( ++uiid_ );
	addRichLabel_->setSize( { 100, 30 } );
	addRichLabel_->setBottomOf( addCheckbox_ );
	addRichLabel_->setAlignedOf( addCheckbox_ );
	addRichLabel_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addRichLabel_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_richlabel, this );

	// addSlider_
	addSlider_ = new dx::Button( );
	addSlider_->setText( "Slider" );
	addSlider_->setUIID( ++uiid_ );
	addSlider_->setSize( { 100, 30 } );
	addSlider_->setBottomOf( addRichLabel_ );
	addSlider_->setAlignedOf( addRichLabel_ );
	addSlider_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addSlider_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_slider, this );

	// Third row
	// addTab_
	addTab_ = new dx::Button( );
	addTab_->setText( "Tab" );
	addTab_->setUIID( ++uiid_ );
	addTab_->setSize( { 100, 30 } );
	addTab_->setBottomOf( addSlider_ );
	addTab_->setAlignedOf( addSlider_ );
	addTab_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addTab_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_tab, this );

	// addTabControl_
	addTabControl_ = new dx::Button( );
	addTabControl_->setText( "Tab Control" );
	addTabControl_->setUIID( ++uiid_ );
	addTabControl_->setSize( { 100, 30 } );
	addTabControl_->setBottomOf( addTab_ );
	addTabControl_->setAlignedOf( addTab_ );
	addTabControl_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addTabControl_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_tabcontrol, this );

	// Fourth row
	// addTextbox_
	addTextbox_ = new dx::Button( );
	addTextbox_->setText( "Textbox" );
	addTextbox_->setUIID( ++uiid_ );
	addTextbox_->setSize( { 100, 30 } );
	addTextbox_->setBottomOf( addTabControl_ );
	addTextbox_->setAlignedOf( addTabControl_ );
	addTextbox_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	addTextbox_->OnMouseReleased( ) += BIND_METHOD_1( &Editor::add_textbox, this );

	removeCurrent_ = new dx::Button( );
	removeCurrent_->setText( "Remove Selected (Delete)" );
	removeCurrent_->setUIID( ++uiid_ );
	removeCurrent_->setSize( { 150, 30 } );
	removeCurrent_->setRightOf( addButton_ );
	removeCurrent_->setAlignedOf( addButton_ );
	removeCurrent_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	removeCurrent_->OnMouseReleased( ) += [this]( dx::Component *sender ) { this->delete_current( ); };

	export_ = new dx::Button( );
	export_->setText( "Export" );
	export_->setUIID( ++uiid_ );
	export_->setSize( { 100, 30 } );
	export_->setLocalPosition( { 5, window_->Height( ) - 35 } );
	export_->OnMouseReleased( ) += [this]( dx::Component *sender ) 
	{
		dx::Application::get( )->BeginInvoke( [this]() 
		{
			auto exporter = new Exporter( );
			exporter->InitializeComponent( this );
		} );
	};

	control_->addTab( fields_ );
	control_->addTab( events_ );

	datas_ = new FieldAndEventDatas( this );
	window_->ForcePaint( );
}

void Editor::Paint(dx::Window * sender, dx::BasePainter * painter)
{
	// Paint background
	painter->PaintRect( { { 0, 0 }, { sender->Width( ), sender->Height( ) } }, dx::Pen( 0xFF222222, 1 ) );
	painter->PaintLine( dx::Line( { screen_.x - 350, 0 }, { screen_.x - 350, screen_.y }, dx::Pen( dx::Colors::White, 2 ) ) );
	painter->PaintLine( dx::Line( { screen_.x - 350, screen_.y / 2 - 25 }, { screen_.x, screen_.y / 2 - 25 }, dx::Pen( dx::Colors::White, 2 ) ) );

	addButton_->Paint( sender, painter );
	addCheckbox_->Paint( sender, painter );
	addRichLabel_->Paint( sender, painter );
	addSlider_->Paint( sender, painter );
	addTab_->Paint( sender, painter );
	addTabControl_->Paint( sender, painter );
	addTextbox_->Paint( sender, painter );

	control_->Paint( sender, painter );
	export_->Paint( sender, painter );

	// Paint 'fake' window
	painter->PaintRect( { { 15, 15 }, { screen_.x * 0.75f, screen_.y * 0.75f } }, dx::Pen( 0xFF2d2d2d ) );


	for ( auto &x : added_ )
	{
		auto compData = (CompData*)x->getUserdata( );
		
		if ( compData->tab_ )
		{
			if ( compData->tab_->isVisible( ) )
				x->Paint( sender, painter );
		}
		else
		{
			x->Paint( sender, painter );
		}
		
		if ( compData->type == CompData::TabControl )
		{
			auto size = x->getSize( );
			auto pos = x->determineRegion( );
			auto left = pos.position.x;
			auto right = left + (size.x < 600 ? 600 : size.x + 25);

			auto top = pos.position.y;
			auto bottom = top + 400;

			auto left_to_right_top = dx::Line( { left, top }, { right, top }, dx::Pen( 0xFF4B4B4B ) );
			auto left_to_right_bottom = dx::Line( { left, bottom }, { right, bottom }, dx::Pen( 0xFF4B4B4B ) );
			auto left_to_bottom = dx::Line( { left, top }, { left, bottom }, dx::Pen( 0xFF4B4B4B ) );
			auto right_to_bottom = dx::Line( { right, top }, { right, bottom }, dx::Pen( 0xFF4B4B4B ) );

			painter->PaintLine( left_to_right_top );
			painter->PaintLine( left_to_bottom );
			painter->PaintLine( left_to_right_bottom );
			painter->PaintLine( right_to_bottom );
		}
	}

	if ( current_ )
	{
		auto compData = (CompData*)current_->getUserdata( );
		auto pos = current_->determineRegion( );

		if ( compData->type == CompData::TabControl )
			pos.size = get_tabctrl_size( current_ );
		
		// Convience
		auto left = pos.position.x;
		auto right = left + pos.size.x;

		auto top = pos.position.y;
		auto bottom = top + pos.size.y;

		auto left_to_right_top = dx::Line( { left, top }, { right, top }, dx::Pen( ) );
		auto left_to_right_bottom = dx::Line( { left, bottom }, { right, bottom }, dx::Pen( ) );
		auto left_to_bottom = dx::Line( { left, top }, { left, bottom }, dx::Pen( ) );
		auto right_to_bottom = dx::Line( { right, top }, { right, bottom }, dx::Pen( ) );

		painter->PaintLine( left_to_right_top );
		painter->PaintLine( left_to_bottom );
		painter->PaintLine( left_to_right_bottom );
		painter->PaintLine( right_to_bottom );
	}
}

void Editor::KeyDown(dx::Window * sender, dx::KeyDownArgs & args)
{
	if ( args.key_code == dx::key_delete )
	{
		delete_current( );
		args.handled = true;
	}
	else if ( args.key_code == dx::key_s && sender->isCtrlHeld( ) )
	{
		save( );
		args.handled = true;
	}
}

void Editor::KeyUp(dx::Window * sender, dx::KeyUpArgs & args)
{
}

void Editor::KeyDownChar(dx::Window * sender, dx::KeyDownCharArgs & args)
{
	control_->KeyDownChar( sender, args );
}

void Editor::MouseMoved(dx::Window * sender, dx::MouseMovedArgs & args)
{
	addButton_->MouseMoved( sender, args );
	addCheckbox_->MouseMoved( sender, args );
	addRichLabel_->MouseMoved( sender, args );
	addSlider_->MouseMoved( sender, args );
	addTab_->MouseMoved( sender, args );
	addTabControl_->MouseMoved( sender, args );
	addTextbox_->MouseMoved( sender, args );
	control_->MouseMoved( sender, args );
	export_->MouseMoved( sender, args );

	for ( auto &x : added_ )
	{
		auto compData = (CompData*)x->getUserdata( );

		if ( compData->dragging_ )
		{
			if ( compData->moved_.x != 0 && compData->moved_.y != 0 )
			{
				auto pos = x->determineRegion( );
				auto increment = (args.position - pos.position) - (compData->moved_ - pos.position);

				x->setLocalPosition( pos.position + increment );

				if ( compData->type == CompData::TabControl )
				{
					for ( auto &item : added_ )
					{ 
						auto itemData = (CompData*)item->getUserdata( );
						if ( itemData->tab_ && itemData->tab_->getParent( ) == x )
						{
							auto itempos = item->determineRegion( );
							itempos.position -= pos.position;
							itempos.position += x->getLocalPosition( );
							
							item->setLocalPosition( itempos.position );
						}
					}
				}
			}
			compData->moved_ = args.position;
			args.handled = true;
			break;
		}
	}
}

void Editor::MouseClicked(dx::Window * sender, dx::MouseClickedArgs & args)
{
	addButton_->MouseClicked( sender, args );
	addCheckbox_->MouseClicked( sender, args );
	addRichLabel_->MouseClicked( sender, args );
	addSlider_->MouseClicked( sender, args );
	addTab_->MouseClicked( sender, args );
	addTabControl_->MouseClicked( sender, args );
	addTextbox_->MouseClicked( sender, args );
	control_->MouseClicked( sender, args );
	export_->MouseClicked( sender, args );

	for ( auto it = added_.rbegin( ), end = added_.rend( ); it < end; ++it )
	{
		auto &x = *it;
		auto compData = (CompData*)x->getUserdata( );
		
		if ( compData->type == CompData::TabControl )
		{ 
			auto pos = x->determineRegion( );
			pos.size = get_tabctrl_size( x );

			if ( args.position.Intersects( pos ) )
			{
				select_item( x );
				compData->dragging_ = true;
				args.handled = true;
			}

			bool dob = false;
			for ( auto &tab : x->getChildren( ) )
			{
				if ( tab->Collides( args.position ) )
				{
					select_item( tab );
					((dx::TabControl*)x)->select( (dx::Tab*)tab );
					args.handled = true;
					dob = true;
				}
			}

			if ( dob )
				break;
		}
		else if ( x->Collides( args.position ) )
		{
			if ( compData->tab_ )
			{
				if ( compData->tab_ != selected_tab( ) )
					break;
			}
			select_item( x );
			compData->dragging_ = true;
			args.handled = true;
			break;
		}
		else
			compData->moved_ = { 0, 0 };
	}

	if ( !args.handled && current_ )
	{ 
		deselect( );
		sender->ForcePaint( );
	}
	else
		unsave( );
}

void Editor::MouseReleased(dx::Window * sender, dx::MouseReleasedArgs & args)
{
	addButton_->MouseReleased( sender, args );
	addCheckbox_->MouseReleased( sender, args );
	addRichLabel_->MouseReleased( sender, args );
	addSlider_->MouseReleased( sender, args );
	addTab_->MouseReleased( sender, args );
	addTabControl_->MouseReleased( sender, args );
	addTextbox_->MouseReleased( sender, args );
	control_->MouseReleased( sender, args );
	export_->MouseReleased( sender, args );

	for ( auto &x : added_ )
	{
		((CompData*)x->getUserdata( ))->dragging_ = false;
		((CompData*)x->getUserdata( ))->moved_ = { 0, 0 };
	}
}

void Editor::MouseScrolled(dx::Window * sender, dx::ScrollArgs & args)
{
}

void Editor::WindowResize(dx::Window * sender, dx::WindowResizeArgs & args)
{
	screen_ = args.region.size;
	args.handled = true;
}

void Editor::add_button(dx::Component * sender)
{
	dx::Button *item = new dx::Button( );
	item->setText( "A Button" );
	item->setUIID( ++uiid_ );
	item->setLocalRegion( { { 30, 65 }, { 100, 30 } } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->type = CompData::Button;
	data->name_ = "button_" + dx::to_string( ++button_c_ );
	item->setUserdata( data );
	added_.emplace_back( std::move( item ) );

	CompData *cdata = current_ ? (CompData*)current_->getUserdata( ) : nullptr;
	if ( cdata && cdata->type == cdata->Tab )
		data->tab_ = (dx::Tab*)current_;

	unsave( );
}

void Editor::add_checkbox(dx::Component * sender)
{
	dx::Checkbox *item = new dx::Checkbox( );
	item->setText( "A Checkbox" );
	item->setUIID( ++uiid_ );
	item->setLocalRegion( { { 30, 65 }, { 25, 25 } } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->type = CompData::Checkbox;
	data->name_ = "checkbox_" + dx::to_string( ++checkbox_c_ );
	item->setUserdata( data );
	added_.emplace_back( std::move( item ) );

	CompData *cdata = current_ ? (CompData*)current_->getUserdata( ) : nullptr;
	if ( cdata && cdata->type == cdata->Tab )
		data->tab_ = (dx::Tab*)current_;

	unsave( );
}

void Editor::add_richlabel(dx::Component * sender)
{
	dx::RichLabel *item = new dx::RichLabel( );
	item->appendText( "A", dx::Colors::Green );
	item->appendText( " RichLabel", dx::Colors::Gold );
	item->setUIID( ++uiid_ );
	item->setLocalPosition( { 30, 65 } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->type = CompData::RichLabel;
	data->name_ = "richlabel_" + dx::to_string( ++richlabel_c_ );
	item->setUserdata( data );
	added_.emplace_back( std::move( item ) );

	CompData *cdata = current_ ? (CompData*)current_->getUserdata( ) : nullptr;
	if ( cdata && cdata->type == cdata->Tab )
		data->tab_ = (dx::Tab*)current_;

	unsave( );
}

void Editor::add_slider(dx::Component * sender)
{
	dx::Slider *item = new dx::Slider( );
	item->setUIID( ++uiid_ );
	item->setLocalRegion( { { 30, 65 }, { 100, 30 } } );
	item->setWheelSize( { 10, 30 } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->type = CompData::Slider;
	data->name_ = "slider_" + dx::to_string( ++slider_c_ );
	item->setUserdata( data );
	added_.emplace_back( std::move( item ) );

	CompData *cdata = current_ ? (CompData*)current_->getUserdata( ) : nullptr;
	if ( cdata && cdata->type == cdata->Tab )
		data->tab_ = (dx::Tab*)current_;

	unsave( );
}

void Editor::add_tab(dx::Component * sender)
{
	if ( !current_ )
	{
		dx::MsgBox( "You must select a TabControl before adding a tab.", "Error", dx::IconError | dx::OK ).Show( );
		return;
	}

	auto compData = (CompData*)current_->getUserdata( );
	if ( compData->type != CompData::TabControl )
	{
		dx::MsgBox( "You must select a TabControl before adding a tab.", "Error", dx::IconError | dx::OK ).Show( );
		return;
	}
	auto item = new dx::Tab( );
	item->setText( "A Tab" );
	item->setUIID( ++uiid_ );
	item->setSize( { 100, 30 } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->name_ = "tab_" + dx::to_string( ++tab_c_ );
	data->type = CompData::Tab;
	item->setUserdata( data );
	((dx::TabControl*)current_)->addTab( item );

	unsave( );
}

void Editor::add_tabcontrol(dx::Component * sender)
{
	auto item = new dx::TabControl( );
	item->setLocalPosition( { 30, 65 } );
	auto data = new CompData( );
	data->type = CompData::TabControl;
	data->name_ = "tabcontrol_" + dx::to_string( ++tabctrl_c_ );
	item->setUserdata( data );
	added_.emplace_back( item );

	current_ = item;
	unsave( );
}

void Editor::add_textbox(dx::Component * sender)
{
	dx::Textbox *item = new dx::Textbox( );
	item->setText( "A Textbox" );
	item->setUIID( ++uiid_ );
	item->setLocalRegion( { { 30, 65 }, { 100, 30 } } );
	item->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	auto data = new CompData( );
	data->type = CompData::Textbox;
	data->name_ = "textbox_" + dx::to_string( ++textbox_c_ );
	item->setUserdata( data );
	added_.emplace_back( std::move( item ) );

	CompData *cdata = current_ ? (CompData*)current_->getUserdata( ) : nullptr;
	if ( cdata && cdata->type == cdata->Tab )
		data->tab_ = (dx::Tab*)current_;

	unsave( );
}

void Editor::select_item(dx::Component * sender)
{
	if ( sender == current_ )
		return;
	
	auto compData = (CompData*)sender->getUserdata( );

	// Customs
	deselect( );
	current_ = sender;
	type_ = compData->type;

	switch( type_ )
	{
	case CompData::Button:
		select_button( );
		break;
	case CompData::Checkbox:
		select_checkbox( );
		break;
	case CompData::RichLabel:
		select_richlabel( );
		break;
	case CompData::Slider:
		select_slider( );
		break;
	case CompData::Tab:
		select_tab( );
		break;
	case CompData::TabControl:
		select_tabcontrol( );
		break;
	case CompData::Textbox:
		select_textbox( );
		break;
	default:
		deselect( );
	}
}

void Editor::deselect()
{
	events_->clear( );
	fields_->clear( );

	eventsComponents_.clear( );
	fieldsComponents_.clear( );

	if ( current_ )
		((CompData*)current_->getUserdata( ))->dragging_ = false;
	current_ = nullptr;
	type_ = 0;
}

void Editor::select_button()
{
	fieldsComponents_ = datas_->fields_when_button;
	eventsComponents_ = datas_->events_when_button;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}

	for ( auto &x : eventsComponents_ )
		events_->addComponent( x );
}

void Editor::select_checkbox()
{
	fieldsComponents_ = datas_->fields_when_checkbox;
	eventsComponents_ = datas_->events_when_checkbox;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}

	for ( auto &x : eventsComponents_ )
		events_->addComponent( x );
}

void Editor::select_richlabel()
{
	fieldsComponents_ = datas_->fields_when_richlabel;
	eventsComponents_ = datas_->events_when_richlabel;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}

	for ( auto &x : eventsComponents_ )
		events_->addComponent( x );
}

void Editor::select_slider()
{
	fieldsComponents_ = datas_->fields_when_slider;
	eventsComponents_ = datas_->events_when_slider;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}

	for ( auto &x : eventsComponents_ )
		events_->addComponent( x );
}

void Editor::select_tab()
{
	fieldsComponents_ = datas_->fields_when_tab;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}
}

void Editor::select_tabcontrol()
{
	fieldsComponents_ = datas_->fields_when_tab;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}
}

void Editor::select_textbox()
{
	fieldsComponents_ = datas_->fields_when_textbox;
	eventsComponents_ = datas_->events_when_textbox;

	fields_->clear( );
	events_->clear( );

	for ( auto &x : fieldsComponents_ )
	{ 
		if ( x->getUserdata( ) )
			((dx::Textbox*)x)->OnCharacterAdded( ).Invoke( x, '\x1' );
		fields_->addComponent( x );
	}

	for ( auto &x : eventsComponents_ )
		events_->addComponent( x );
}

void Editor::delete_current()
{
	for ( auto it = added_.begin( ), end = added_.end( ); it < end; ++it )
	{ 
		auto &x = *it;
		if ( x == current_ )
		{
			added_.erase( it );
			break;
		}
	}


	if ( current_ )
	{
		auto compData = (CompData*)current_->getUserdata( );
		if ( compData->type == compData->Tab )
			current_->getParent( )->remove_child( current_ );

		remove_events( current_ );
		delete current_;
		current_ = nullptr;
		deselect( );
	}

	unsave( );
}

void Editor::unsave()
{
	unsaved_ = true;
	window_->setTitle( "DX Editor | *.dxproj*" );
}

void Editor::save()
{
	unsaved_ = false;
	window_->setTitle( "DX Editor | *.dxproj" );
}

dx::Tab * Editor::selected_tab()
{
	for ( auto &x : added_ )
	{
		auto compData = (CompData*)x->getUserdata( );
		if ( compData->type == CompData::TabControl )
			return ((dx::TabControl*)x)->selected( );
	}
	return nullptr;
}

void Editor::remove_events(dx::Component *component)
{
	for ( auto it = customEvents_.begin( ), end = customEvents_.end( ); it < end; ++it )
	{
		auto &x = *it;
		if ( x.component == component )
		{
			customEvents_.erase( it );
			// Recursively remove the next items
			return remove_events( component );
		}
	}
}

dx::Vector2 Editor::get_tabctrl_size( dx::Component *component )
{
	return std::move( ((dx::TabControl*)component)->get_tab_size( ) );
}

Editor::FieldAndEventDatas::FieldAndEventDatas( Editor *editor )
{
	// Fields when button
	{
		auto *name = new dx::Textbox( );
		name->setText( "name" );
		name->setLooseFocusKey( '\r' );
		name->setUIID( ++editor->uiid_ );
		name->setBottomOf( editor->fields_ );
		name->setAlignedOf( editor->fields_ );
		name->setSize( { 125, 30 } );
		name->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		name->setUserdata( (void*)1 );
		name->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if ( key == '\x1' )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				sender->setText( compData->name_ );
			}
		};
		name->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				compData->name_ = sender->getText( );
				editor->window_->ForcePaint( );
			}
		};

		auto *text = new dx::Textbox( );
		text->setText( "A Button" );
		text->setLooseFocusKey( '\r' );
		text->setUIID( ++editor->uiid_ );
		text->setBottomOf( name );
		text->setAlignedOf( name );
		text->setSize( { 125, 30 } );
		text->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		text->setUserdata( (void*)1 );
		text->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( editor->current_->getText( ) );
		};
		text->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				editor->current_->setText( sender->getText( ) );
				editor->window_->ForcePaint( );
			}
			
		};
		
		auto uiid = new dx::Textbox( );
		uiid->setText( dx::to_string( ++editor->uiid_ ) );
		uiid->setFilter( "1234567890" );
		uiid->setLooseFocusKey( '\r' );
		uiid->setUIID( editor->uiid_ );
		uiid->setBottomOf( text );
		uiid->setAlignedOf( text );
		uiid->setSize( { 65, 30 } );
		uiid->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		uiid->setUserdata( (void*)1 );
		uiid->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText(  dx::to_string( editor->current_->getUIID( ) ) );
		};
		uiid->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				editor->current_->setUIID( sender->getText( ).to<dx::uint>( ) );
				editor->window_->ForcePaint( );
			}
		};

		auto labelWidth = new dx::RichLabel( );
		labelWidth->appendText( "Width: ", dx::Colors::White );
		labelWidth->setUIID( ++editor->uiid_ );
		labelWidth->setBottomOf( uiid );
		labelWidth->setAlignedOf( uiid );
		//labelWidth->setSize( { 65, 20 } );
		
		auto sizeX = new dx::Textbox( );
		sizeX->setText( "100" );
		sizeX->setLooseFocusKey( '\r' );
		sizeX->setFilter( "1234567890." );
		sizeX->setUIID( ++editor->uiid_ );
		sizeX->setBottomOf( labelWidth );
		sizeX->setAlignedOf( labelWidth );
		sizeX->setSize( { 65, 30 } );
		sizeX->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		sizeX->setUserdata( (void*)1 );
		sizeX->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( editor->current_->getSize( ).x ) );
		};
		sizeX->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto size = editor->current_->getSize( );
				size.x = sender->getText( ).to<dx::uint>( );
				editor->current_->setSize( size );
				editor->window_->ForcePaint( );
			}
		};

		auto labelHeight = new dx::RichLabel( );
		labelHeight->appendText( "Height: ", dx::Colors::White );
		labelHeight->setUIID( ++editor->uiid_ );
		labelHeight->setBottomOf( sizeX );
		labelHeight->setAlignedOf( labelWidth );
		//labelHeight->setSize( { 65, 20 } );
		
		auto sizeY = new dx::Textbox( );
		sizeY->setText( "30" );
		sizeY->setLooseFocusKey( '\r' );
		sizeY->setFilter( "1234567890." );
		sizeY->setUIID( ++editor->uiid_ );
		sizeY->setBottomOf( labelHeight );
		sizeY->setAlignedOf( labelHeight );
		sizeY->setSize( { 65, 30 } );
		sizeY->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		sizeX->setUserdata( (void*)1 );
		sizeY->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( editor->current_->getSize( ).y ) );
		};
		sizeY->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto size = editor->current_->getSize( );
				size.y = sender->getText( ).to<dx::uint>( );
				editor->current_->setSize( size );
				editor->window_->ForcePaint( );
			}
		};

		
		fields_when_button.emplace_back( name );
		fields_when_button.emplace_back( text );
		fields_when_button.emplace_back( uiid );
		fields_when_button.emplace_back( labelWidth );
		fields_when_button.emplace_back( sizeX );
		fields_when_button.emplace_back( labelHeight );
		fields_when_button.emplace_back( sizeY );
	}

	// Global events
	auto eventPressed = new dx::RichLabel( );
	eventPressed->appendText( "OnMousePressed: " );
	eventPressed->setUIID( ++editor->uiid_ );
	eventPressed->setBottomOf( editor->fields_ );
	eventPressed->setAlignedOf( editor->fields_ );

	auto eventReleased = new dx::RichLabel( );
	eventReleased->appendText( "OnMouseReleased: " );
	eventReleased->setUIID( ++editor->uiid_ );
	eventReleased->setBottomOf( eventPressed );
	eventReleased->setAlignedOf( editor->fields_ );

	auto eventEnter = new dx::RichLabel( );
	eventEnter->appendText( "OnMouseEnter: " );
	eventEnter->setUIID( ++editor->uiid_ );
	eventEnter->setBottomOf( eventReleased );
	eventEnter->setAlignedOf( editor->fields_ );
	
	auto eventLeave = new dx::RichLabel( );
	eventLeave->appendText( "OnMouseLeave: " );
	eventLeave->setUIID( ++editor->uiid_ );
	eventLeave->setBottomOf( eventEnter );
	eventLeave->setAlignedOf( editor->fields_ );

	// Events button
	{	
		auto handle_pressed = new dx::Button( );
		handle_pressed->setText( "Handle" );
		handle_pressed->setUIID( ++editor->uiid_ );
		handle_pressed->setRightOf( eventPressed );
		handle_pressed->setAlignedOf( eventPressed );
		handle_pressed->setSize( { 100, 25 } );
		handle_pressed->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handle_pressed->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnMousePressed";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};

		auto handle_release = new dx::Button( );
		handle_release->setText( "Handle" );
		handle_release->setUIID( ++editor->uiid_ );
		handle_release->setRightOf( eventReleased );
		handle_release->setAlignedOf( eventReleased );
		handle_release->setSize( { 100, 25 } );
		handle_release->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handle_release->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnMouseReleased";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};

		auto handle_enter = new dx::Button( );
		handle_enter->setText( "Handle" );
		handle_enter->setUIID( ++editor->uiid_ );
		handle_enter->setRightOf( eventEnter );
		handle_enter->setAlignedOf( eventEnter );
		handle_enter->setSize( { 100, 25 } );
		handle_enter->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handle_enter->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnMouseEnter";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};

		auto handle_leave = new dx::Button( );
		handle_leave->setText( "Handle" );
		handle_leave->setUIID( ++editor->uiid_ );
		handle_leave->setRightOf( eventLeave );
		handle_leave->setAlignedOf( eventLeave );
		handle_leave->setSize( { 100, 25 } );
		handle_leave->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handle_leave->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnMouseLeave";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};

		events_when_button.emplace_back( eventPressed );
		events_when_button.emplace_back( handle_pressed );
		events_when_button.emplace_back( eventReleased );
		events_when_button.emplace_back( handle_release );
		events_when_button.emplace_back( eventEnter );
		events_when_button.emplace_back( handle_enter );
		events_when_button.emplace_back( eventLeave );
		events_when_button.emplace_back( handle_leave );
	}


	fields_when_checkbox = fields_when_button;
	events_when_checkbox = events_when_button;

	// Add some more stuff to slider
	{
		auto *name = new dx::Textbox( );
		name->setText( "name" );
		name->setLooseFocusKey( '\r' );
		name->setUIID( ++editor->uiid_ );
		name->setBottomOf( editor->fields_ );
		name->setAlignedOf( editor->fields_ );
		name->setSize( { 125, 30 } );
		name->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		name->setUserdata( (void*)1 );
		name->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if ( key == '\x1' )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				sender->setText( compData->name_ );
			}
		};
		name->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				compData->name_ = sender->getText( );
				editor->window_->ForcePaint( );
			}
		};

		auto uiid = new dx::Textbox( );
		uiid->setText( dx::to_string( ++editor->uiid_ ) );
		uiid->setFilter( "1234567890" );
		uiid->setLooseFocusKey( '\r' );
		uiid->setUIID( editor->uiid_ );
		uiid->setBottomOf( name );
		uiid->setAlignedOf( name );
		uiid->setSize( { 65, 30 } );
		uiid->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		uiid->setUserdata( (void*)1 );
		uiid->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText(  dx::to_string( editor->current_->getUIID( ) ) );
		};
		uiid->OnPrePaint( ) += [editor = editor]( dx::Component *sender, dx::BasePainter* )mutable
		{
			sender->setText( dx::to_string( editor->current_->getUIID( ) ) );
		};
		uiid->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				editor->current_->setUIID( sender->getText( ).to<dx::uint>( ) );
				editor->window_->ForcePaint( );
			}
		};

		auto labelWidth = new dx::RichLabel( );
		labelWidth->appendText( "Width: ", dx::Colors::White );
		labelWidth->setUIID( ++editor->uiid_ );
		labelWidth->setBottomOf( uiid );
		labelWidth->setAlignedOf( uiid );
		//labelWidth->setSize( { 65, 20 } );
		
		auto sizeX = new dx::Textbox( );
		sizeX->setText( "100" );
		sizeX->setLooseFocusKey( '\r' );
		sizeX->setFilter( "1234567890." );
		sizeX->setUIID( ++editor->uiid_ );
		sizeX->setBottomOf( labelWidth );
		sizeX->setAlignedOf( labelWidth );
		sizeX->setSize( { 65, 30 } );
		sizeX->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		sizeX->setUserdata( (void*)1 );
		sizeX->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( editor->current_->getSize( ).x ) );
		};
		sizeX->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto size = editor->current_->getSize( );
				size.x = sender->getText( ).to<dx::uint>( );
				editor->current_->setSize( size );
				editor->window_->ForcePaint( );
			}
		};

		auto labelHeight = new dx::RichLabel( );
		labelHeight->appendText( "Height: ", dx::Colors::White );
		labelHeight->setUIID( ++editor->uiid_ );
		labelHeight->setBottomOf( sizeX );
		labelHeight->setAlignedOf( labelWidth );
		//labelHeight->setSize( { 65, 20 } );
		
		auto sizeY = new dx::Textbox( );
		sizeY->setText( "30" );
		sizeY->setLooseFocusKey( '\r' );
		sizeY->setFilter( "1234567890." );
		sizeY->setUIID( ++editor->uiid_ );
		sizeY->setBottomOf( labelHeight );
		sizeY->setAlignedOf( labelHeight );
		sizeY->setSize( { 65, 30 } );
		sizeY->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		sizeY->setUserdata( (void*)1 );
		sizeY->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( editor->current_->getSize( ).y ) );
		};
		sizeY->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto size = editor->current_->getSize( );
				size.y = sender->getText( ).to<dx::uint>( );
				editor->current_->setSize( size );
				((dx::Slider*)editor->current_)->setWheelSize( { 10, size.y } );
				editor->window_->ForcePaint( );
			}
		};

		auto labelMax = new dx::RichLabel( );
		labelMax->appendText( "Max Delta: ", dx::Colors::White );
		labelMax->setUIID( ++editor->uiid_ );
		labelMax->setBottomOf( sizeY );
		labelMax->setAlignedOf( sizeY );
		
		auto maxValue = new dx::Textbox( );
		maxValue->setText( "100" );
		maxValue->setLooseFocusKey( '\r' );
		maxValue->setFilter( "1234567890." );
		maxValue->setUIID( ++editor->uiid_ );
		maxValue->setBottomOf( labelMax );
		maxValue->setAlignedOf( labelMax );
		maxValue->setSize( { 65, 30 } );
		maxValue->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		maxValue->setUserdata( (void*)1 );
		maxValue->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( ((dx::Slider*)editor->current_)->getMaxDelta( ) ) );
		};
		maxValue->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			((dx::Slider*)editor->current_)->setMaxDelta( sender->getText( ).to<dx::uint>( ) );
			editor->window_->ForcePaint( );
		};

		// Value
		auto labelValue = new dx::RichLabel( );
		labelValue->appendText( "Value: ", dx::Colors::White );
		labelValue->setUIID( ++editor->uiid_ );
		labelValue->setBottomOf( maxValue );
		labelValue->setAlignedOf( maxValue );
		

		auto value = new dx::Textbox( );
		value->setText( "0" );
		value->setLooseFocusKey( '\r' );
		value->setFilter( "1234567890." );
		value->setUIID( ++editor->uiid_ );
		value->setBottomOf( labelValue );
		value->setAlignedOf( labelValue );
		value->setSize( { 65, 30 } );
		value->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		value->setUserdata( (void*)1 );
		value->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if (  key == '\x1')
				sender->setText( dx::to_string( ((dx::Slider*)editor->current_)->getDelta( ).x ) );
		};
		value->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			((dx::Slider*)editor->current_)->setDelta( sender->getText( ).to<float>( ) );
			((dx::Slider*)editor->current_)->moveWheelToDelta( );
			editor->window_->ForcePaint( );
		};

		fields_when_slider.emplace_back( name );
		fields_when_slider.emplace_back( uiid );
		fields_when_slider.emplace_back( labelWidth );
		fields_when_slider.emplace_back( sizeX );
		fields_when_slider.emplace_back( labelHeight );
		fields_when_slider.emplace_back( sizeY );
		fields_when_slider.emplace_back( labelMax );
		fields_when_slider.emplace_back( maxValue );
		fields_when_slider.emplace_back( labelValue );
		fields_when_slider.emplace_back( value );
	}

	fields_when_tab.emplace_back( fields_when_button.front( ) );
	fields_when_tab.emplace_back( *(fields_when_button.begin( ) + 1) );
	fields_when_richlabel.emplace_back( fields_when_button.front( ) );
	fields_when_textbox = fields_when_button;
	events_when_textbox = events_when_button;

	// Tabctronol name
	{
		auto *name = new dx::Textbox( );
		name->setText( "name" );
		name->setLooseFocusKey( '\r' );
		name->setUIID( ++editor->uiid_ );
		name->setBottomOf( editor->fields_ );
		name->setAlignedOf( editor->fields_ );
		name->setSize( { 125, 30 } );
		name->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		name->setUserdata( (void*)1 );
		name->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if ( key == '\x1' )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				sender->setText( compData->name_ );
			}
		};
		name->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto compData = (CompData*)editor->current_->getUserdata( );
				compData->name_ = sender->getText( );
				editor->window_->ForcePaint( );
			}
		};

		fields_when_tabcontrol.emplace_back( name );
	}


	// Extra events for textbox
	{
		auto labelOnGainFocus = new dx::RichLabel( );
		labelOnGainFocus->appendText( "OnGainFocus: " );
		labelOnGainFocus->setUIID( ++editor->uiid_ );
		labelOnGainFocus->setBottomOf( *std::prev( std::prev( events_when_textbox.end( ) ) ) );
		labelOnGainFocus->setAlignedOf( *std::prev( std::prev( events_when_textbox.end( ) ) ) );

		auto handleGain = new dx::Button( );
		handleGain->setText( "Handle" );
		handleGain->setUIID( ++editor->uiid_ );
		handleGain->setRightOf( labelOnGainFocus );
		handleGain->setAlignedOf( labelOnGainFocus );
		handleGain->setSize( { 100, 25 } );
		handleGain->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handleGain->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnGainFocus";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};


		auto labelOnLostFocus = new dx::RichLabel( );
		labelOnLostFocus->appendText( "OnLostFocus: " );
		labelOnLostFocus->setUIID( ++editor->uiid_ );
		labelOnLostFocus->setBottomOf( labelOnGainFocus );
		labelOnLostFocus->setAlignedOf( labelOnGainFocus );

		auto handleLost = new dx::Button( );
		handleLost->setText( "Handle" );
		handleLost->setUIID( ++editor->uiid_ );
		handleLost->setRightOf( labelOnLostFocus );
		handleLost->setAlignedOf( labelOnLostFocus );
		handleLost->setSize( { 100, 25 } );
		handleLost->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		handleLost->OnMouseReleased( ) += [editor = editor]( dx::Component *sender )
		{
			EventWrapper wrapper;
			wrapper.component = editor->current_;
			wrapper.event_name = "OnLostFocus";
			editor->customEvents_.emplace_back( std::move( wrapper ) );
		};

		events_when_textbox.emplace_back( labelOnGainFocus );
		events_when_textbox.emplace_back( handleGain );
		events_when_textbox.emplace_back( labelOnLostFocus );
		events_when_textbox.emplace_back( handleLost );
	}

	fields_when_richlabel.emplace_back( fields_when_button.front( ) );
	{
		auto text = new dx::Textbox( );
		text->setText( "text" );
		text->setLooseFocusKey( '\r' );
		text->setUIID( ++editor->uiid_ );
		text->setBottomOf( fields_when_richlabel.front( ) );
		text->setAlignedOf( fields_when_richlabel.front( ) );
		text->setSize( { 125, 30 } );
		text->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
		text->setUserdata( (void*)1 );
		text->OnCharacterAdded( ) += [editor = editor]( dx::Component *sender, const char &key )mutable
		{
			if ( key == '\x1' )
				sender->setText( editor->current_->getText( ) );
		};
		text->OnLostFocus( ) += [editor = editor]( dx::Component *sender )
		{
			if ( editor->current_ )
			{
				auto lbl = (dx::RichLabel*)editor->current_;
				lbl->clearText( );
				lbl->appendText( sender->getText( ) );
			}
		};

		fields_when_richlabel.emplace_back( text );
	}
}

Editor::FieldAndEventDatas::~FieldAndEventDatas()
{
}
