#pragma once
#include <dx/src/api>


// TODO:
//		Multiple selection
//		Grids
//		Cleanup code
//		Remove events when item is removed

class Editor
{
public:
	struct CompData
	{
		enum types
		{
			Button = 1,
			Checkbox,
			RichLabel,
			Slider,
			Tab,
			TabControl,
			Textbox
		};

		dx::String name_;
		bool selected_;
		bool dragging_;
		dx::Tab *tab_;
		dx::Vector2 moved_;
		dx::uint type;
	};

	struct EventWrapper
	{
		dx::String event_name;
		dx::Component *component;
	};

	Editor( );
	~Editor( );

	void InitializeComponent( );

	void Paint( dx::Window *sender, dx::BasePainter *painter );

	void KeyDown( dx::Window *sender, dx::KeyDownArgs &args );

	void KeyUp( dx::Window *sender, dx::KeyUpArgs &args );

	void KeyDownChar( dx::Window *sender, dx::KeyDownCharArgs &args );

	void MouseMoved( dx::Window *sender, dx::MouseMovedArgs &args );

	void MouseClicked( dx::Window *sender, dx::MouseClickedArgs &args );

	void MouseReleased( dx::Window *sender, dx::MouseReleasedArgs &args );

	void MouseScrolled( dx::Window *sender, dx::ScrollArgs &args );

	void WindowResize( dx::Window *sender, dx::WindowResizeArgs &args );

	void add_button( dx::Component *sender );

	void add_checkbox( dx::Component *sender );

	void add_richlabel( dx::Component *sender );

	void add_slider( dx::Component *sender );

	void add_tab( dx::Component *sender );

	void add_tabcontrol( dx::Component *sender );

	void add_textbox( dx::Component *sender );

	void select_item( dx::Component *sender );

	void deselect( );

	void select_button( );

	void select_checkbox( );
	
	void select_richlabel( );

	void select_slider( );

	void select_tab( );

	void select_tabcontrol( );

	void select_textbox( );

	void delete_current( );

	void unsave( );

	void save( );

	dx::Tab *selected_tab( );

	void remove_events( dx::Component *component );

	decltype(auto) get_window( ) { return window_; }
	decltype(auto) get_painter( ) { return painter_; }
	decltype(auto) get_current( ) { return current_; }
	decltype(auto) get_current_type( ) { return type_; }
	decltype(auto) get_added( ) { return added_; }
	decltype(auto) get_screen( ) { return screen_; }
	decltype(auto) get_custom_events( ) { return customEvents_; }

	// Items
private:
	dx::Vector2 get_tabctrl_size( dx::Component *component );

	// Other
	dx::uint uiid_;
	dx::Vector2 screen_;
	bool unsaved_, tab_;

	// Base stuff
	dx::Window *window_;
	dx::BasePainter *painter_;

	// Items that can be added
	dx::Button *addButton_, *addCheckbox_, *addRichLabel_,
		*addSlider_, *addTab_, *addTabControl_, *addTextbox_;
	dx::Button *removeCurrent_;
	dx::uint button_c_, checkbox_c_, richlabel_c_, slider_c_,
			 tab_c_, tabctrl_c_, textbox_c_;

	// Current items
	dx::Component *current_;
	dx::uint type_;

	// Tab control for events and fields
	dx::TabControl *control_;
	dx::Tab *events_, *fields_;
	std::vector<dx::Component*> eventsComponents_, fieldsComponents_;
	
	// Items on 'map'
	std::vector<dx::Component*> added_;
	std::vector<EventWrapper> customEvents_;

	// Export
	dx::Button *export_;

	struct FieldAndEventDatas
	{
		std::vector<dx::Component*> fields_when_button;
		std::vector<dx::Component*> events_when_button;

		std::vector<dx::Component*> fields_when_checkbox;
		std::vector<dx::Component*> events_when_checkbox;

		std::vector<dx::Component*> fields_when_richlabel;
		std::vector<dx::Component*> events_when_richlabel;

		std::vector<dx::Component*> fields_when_slider;
		std::vector<dx::Component*> events_when_slider;

		std::vector<dx::Component*> fields_when_tab;

		std::vector<dx::Component*> fields_when_tabcontrol;

		std::vector<dx::Component*> fields_when_textbox;
		std::vector<dx::Component*> events_when_textbox;

		FieldAndEventDatas( Editor *editor );
		~FieldAndEventDatas( );
	};

	FieldAndEventDatas *datas_;
	 

};
