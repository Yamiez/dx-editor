#include "Editor/Editor.h"
#pragma comment( lib, "dx.lib" )

int main( )
{
	auto appl = dx::Application::Create( );
	appl->setTickRate( 1 );

	Editor editor;
	editor.InitializeComponent( );
	
	appl->run( );
}