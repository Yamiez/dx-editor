#pragma once
#include <dx\src\api>

class Editor;

// TODO:
//		Add Pragma once
//		dx::Painter::Create line is missing parenthesis
//		Allignments when exporting
//

class Exporter
{
public:
	
	void InitializeComponent( Editor *editor );

	void Paint( dx::Window *sender, dx::BasePainter *painter );

	void KeyDownChar( dx::Window *sender, dx::KeyDownCharArgs &args );

	void MouseMoved( dx::Window *sender, dx::MouseMovedArgs &args );

	void MouseClicked( dx::Window *sender, dx::MouseClickedArgs &args );

	void MouseReleased( dx::Window *sender, dx::MouseReleasedArgs &args );
	
	void export__pressed( dx::Component *sender );

	void export_task( );

	dx::String get_type_str( dx::uint type );

	void setup_type( dx::Component *components, dx::uint type, std::ostream &output );

	void setup_button( dx::Component *components, dx::uint type, std::ostream &output );
	
	void setup_checkbox( dx::Component *components, dx::uint type, std::ostream &output );
	
	void setup_richlabel( dx::Component *components, dx::uint type, std::ostream &output );

	void setup_slider( dx::Component *components, dx::uint type, std::ostream &output );

	void setup_tab( dx::Component *components, dx::uint type, std::ostream &output );

	void setup_tabcontrol( dx::Component *components, dx::uint type, std::ostream &output );

	void setup_textbox( dx::Component *components, dx::uint type, std::ostream &output );

	void generate_function_with_calls( dx::String function_to_call, dx::String args, std::ostream &output );

	bool has_events( dx::Component *component );

	void implement_events( dx::Component *component, std::ostream &out );

private:
	Editor *editor_;
	dx::uint uiid_;
	dx::Window *window_;
	dx::BasePainter *painter_;

	dx::Textbox *path_, *classname_;
	dx::Button *export_;
	
	std::future<void> fut_;
};