#include "Exporter.h"
#include "../Editor/Editor.h"

void Exporter::InitializeComponent(Editor * editor)
{
	editor_ = editor;
	
	window_ = dx::Window::Create( editor_->get_window( ), "DXExporter", "DX Exporter", { { editor_->get_screen( ).x / 2 - 300, editor_->get_screen( ).y / 2 - 200 }, { 600, 400 } } );
	window_->OnPaint( ) += BIND_METHOD_2( &Exporter::Paint, this );
	window_->OnKeyDownChar( ) += BIND_METHOD_2( &Exporter::KeyDownChar, this );
	window_->OnMouseMoved( ) += BIND_METHOD_2( &Exporter::MouseMoved, this );
	window_->OnMouseClicked( ) += BIND_METHOD_2( &Exporter::MouseClicked, this );
	window_->OnMouseReleased( ) += BIND_METHOD_2( &Exporter::MouseReleased, this );
	window_->Show( );
	window_->SpecializePaint( dx::Window::OnTick_t );


	painter_ = (dx::BasePainter*)dx::Painter::Create( window_, true, false );
	auto context = painter_->defaultFont( )->context( );
	context.Quality = PROOF_QUALITY;
	painter_->setDefaultFont( dx::Font::Create( "Consolas", context, painter_ ) );

	path_ = new dx::Textbox( );
	path_->setPromptText( "Path" );
	path_->setUIID( ++uiid_ );
	path_->setLocalRegion( { { window_->Width( ) / 2 - 100, window_->Height( ) / 2 - 15 }, { 200, 30 } } );
	path_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );

	classname_ = new dx::Textbox( );
	classname_->setPromptText( "Export class name" );
	classname_->setUIID( ++uiid_ );
	classname_->setBottomOf( path_ );
	classname_->setAlignedOf( path_ );
	classname_->setSize( { 200, 30 } );
	classname_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );

	export_ = new dx::Button( );
	export_->setText( "Export" );
	export_->setUIID( ++uiid_ );
	export_->setBottomOf( classname_ );
	export_->setAlignedOf( classname_ );
	export_->setSize( { 200, 30 } );
	export_->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );
	export_->OnMouseReleased( ) += BIND_METHOD_1( &Exporter::export__pressed, this );
}

void Exporter::Paint(dx::Window * sender, dx::BasePainter * painter)
{
	// Paint background
	painter->PaintRect( { { 0, 0 }, { sender->Width( ), sender->Height( ) } }, dx::Pen( 0xFF222222, 1 ) );
	
	path_->Paint( sender, painter );
	classname_->Paint( sender, painter );
	export_->Paint( sender, painter );
}

void Exporter::KeyDownChar(dx::Window * sender, dx::KeyDownCharArgs & args)
{
	path_->KeyDownChar( sender, args );
	classname_->KeyDownChar( sender, args );
}

void Exporter::MouseMoved(dx::Window * sender, dx::MouseMovedArgs & args)
{
	path_->MouseMoved( sender, args );
	export_->MouseMoved( sender, args );
	classname_->MouseMoved( sender, args );
}

void Exporter::MouseClicked(dx::Window * sender, dx::MouseClickedArgs & args)
{
	path_->MouseClicked( sender, args );
	export_->MouseClicked( sender, args );
	classname_->MouseClicked( sender, args );
}

void Exporter::MouseReleased(dx::Window * sender, dx::MouseReleasedArgs & args)
{
	path_->MouseReleased( sender, args );
	export_->MouseReleased( sender, args );
	classname_->MouseReleased( sender, args );
}

void Exporter::export__pressed(dx::Component * sender)
{
	if ( !fut_.valid( ) || fut_.wait_for( std::chrono::milliseconds( 0 ) ) == std::future_status::ready)
		fut_ = std::async( std::launch::async, std::mem_fn( &Exporter::export_task ), this );
}

namespace std
{
	std::ostream &tab( std::ostream &oStr )
	{
		oStr.put( '\t' );
		return oStr;
	}

}

void Exporter::export_task()
{
	if ( path_->getText( ).empty( ) )
	{
		dx::MsgBox( "The 'path' component may not be empty.", "Error", dx::DialogType::IconError | dx::DialogType::OK ).Show( );
		return;
	}
	
	if ( classname_->getText( ).empty( ) )
	{
		dx::MsgBox( "The 'Class Name' component may not be empty.", "Error", dx::DialogType::IconError | dx::DialogType::OK ).Show( );
		return;
	}

	std::ofstream stream_header{ (path_->getText( ) + ".h").c_str( ) }, stream_impl{ (path_->getText() + ".cpp").c_str() };
	
	stream_header << "#pragma once" << std::endl;
	stream_header << "#include <dx/src/api>" << std::endl;
	stream_header << std::endl << std::endl << std::endl;
	stream_header << "class " << classname_->getText( ) << std::endl;
	stream_header << "{" << std::endl;
	stream_header << "public:" << std::endl;
	stream_header << std::tab <<  classname_->getText( ) << "( );" << std::endl;
	stream_header << std::tab << "~" << classname_->getText( ) << "( );" << std::endl;
	stream_header << std::endl << std::endl;

	// Standard methods
	stream_header << std::tab << "void InitializeComponent( );" << std::endl;
	stream_header << std::tab << "void Paint( dx::Window *sender, dx::BasePainter *painter );" << std::endl;
	stream_header << std::tab << "void KeyDown( dx::Window *sender, dx::KeyDownArgs &args );" << std::endl;
	stream_header << std::tab << "void KeyUp( dx::Window *sender, dx::KeyUpArgs &args );" << std::endl;
	stream_header << std::tab << "void KeyDownChar( dx::Window *sender, dx::KeyDownCharArgs &args );" << std::endl;
	stream_header << std::tab << "void MouseMoved( dx::Window *sender, dx::MouseMovedArgs &args );" << std::endl;
	stream_header << std::tab << "void MouseClicked( dx::Window *sender, dx::MouseClickedArgs &args );" << std::endl;
	stream_header << std::tab << "void MouseReleased( dx::Window *sender, dx::MouseReleasedArgs &args );" << std::endl;
	stream_header << std::tab << "void MouseScrolled( dx::Window *sender, dx::ScrollArgs &args );" << std::endl;
	
	for ( auto &x : editor_->get_custom_events( ) )
	{
		auto compData = (Editor::CompData*)x.component->getUserdata( );
		stream_header << std::tab << "void " << compData->name_ << "_" << x.event_name << "( dx::Component *sender );" << std::endl;
	}

	stream_header << "private:" << std::endl;
	stream_header << std::tab << "dx::Window *window_;" << std::endl;
	stream_header << std::tab << "dx::BasePainter *painter_;" << std::endl;
	for ( auto &x : editor_->get_added( ) )
	{
		auto compData = (Editor::CompData*)x->getUserdata( );
		stream_header << std::tab << get_type_str( compData->type ) << " *" << compData->name_ << ";" << std::endl;

		if ( compData->type == compData->TabControl )
		{
			for ( auto &child : x->getChildren( ) )
			{
				auto childData = (Editor::CompData*)child->getUserdata( );
				stream_header << std::tab << get_type_str( childData->type ) << " *" << childData->name_ << ";" << std::endl;
			}
		}
	}

	stream_header << "};" << std::endl;


	// Now the impls
	auto class_name = classname_->getText( );

	stream_impl << "#include \"" << path_->getText( ) + ".h" << "\"";
	stream_impl << std::endl << std::endl << std::endl;

	// Constructor
	stream_impl << class_name << "::" << class_name << "( )" << std::endl;
	stream_impl << "\t: ";
	
	auto count = 0u;
	auto added = editor_->get_added( );
	for ( auto it = added.begin( ), end = added.end( ); it < end; ++it )
	{
		auto &x = *it;
		auto compData = (Editor::CompData*)x->getUserdata();

		if ( it == std::prev( end ) )
			stream_impl << compData->name_ << "( nullptr )" << std::endl;
		else
			stream_impl << compData->name_ << "( nullptr ), ";

		if ( count == 3 )
		{
			stream_impl << std::endl << std::tab << "  ";
			++count;
		}

		++count;
	}

	stream_impl << "{ }" << std::endl << std::endl;

	// Destructor
	stream_impl << class_name << "::~" << class_name << "( )" << std::endl;
	stream_impl << "{ }" << std::endl << std::endl;
		
	// Methods
	stream_impl << "void " << class_name << "::InitializeComponent( )" << std::endl;
	stream_impl << "{" << std::endl;
	stream_impl << std::tab << "window_ = dx::Window::Create( \"DXEditorProj\", \"DX Project from Editor | dx\", { { 300, 300 }, { 1280, 720 } } );" << std::endl;
	stream_impl << std::tab << "painter_ = (dx::BasePainter*)dx::Painter::Create( window_ );" << std::endl;
	stream_impl << std::tab << "auto context = painter_->defaultFont( )->context( );" << std::endl;
	stream_impl << std::tab << "context.Quality = PROOF_QUALITY;" << std::endl;
	stream_impl << std::tab << "painter_->setDefaultFont( dx::Font::Create( \"Consolas\", context, painter_ ) );" << std::endl;
	stream_impl << std::tab << "window_->Show( );" << std::endl;
	stream_impl << std::tab << "window_->SpecializePaint( dx::Window::OnEvent_t );" << std::endl;
	stream_impl << std::tab << "window_->OnPaint( ) += BIND_METHOD_2( &" << class_name << "::Paint, this );" << std::endl;
	stream_impl << std::tab << "window_->OnKeyDown( ) += BIND_METHOD_2( &" << class_name << "::KeyDown, this );" << std::endl;
	stream_impl << std::tab << "window_->OnKeyUp( ) += BIND_METHOD_2( &" << class_name << "::KeyUp, this );" << std::endl;
	stream_impl << std::tab << "window_->OnKeyDownChar( ) += BIND_METHOD_2( &" << class_name << "::KeyDownChar, this );" << std::endl;
	stream_impl << std::tab << "window_->OnMouseMoved( ) += BIND_METHOD_2( &" << class_name << "::MouseMoved, this );" << std::endl;
	stream_impl << std::tab << "window_->OnMouseClicked( ) += BIND_METHOD_2( &" << class_name << "::MouseClicked, this );" << std::endl;
	stream_impl << std::tab << "window_->OnMouseReleased( ) += BIND_METHOD_2( &" << class_name << "::MouseReleased, this );" << std::endl;
	
	// Now to the custom variables
	for ( auto it = added.begin( ), end = added.end( ); it < end; ++it )
	{
		auto &x = *it;
		auto compData = (Editor::CompData*)x->getUserdata( );

		stream_impl << std::tab << "// ================ " << compData->name_ << " ================" << std::endl;
		stream_impl << std::tab << compData->name_ << " = new " << get_type_str( compData->type ) << "( );" << std::endl;
		setup_type( x, compData->type, stream_impl );

		if ( compData->type == compData->TabControl )
		{
			for ( auto &child : x->getChildren( ) )
			{
				auto childData = (Editor::CompData*)child->getUserdata( );

				stream_impl << std::tab << "// ================ " << childData->name_ << " ================" << std::endl;
				stream_impl << std::tab << childData->name_ << " = new " << get_type_str( childData->type ) << "( );" << std::endl;
				setup_type( child, childData->type, stream_impl );
			}
		}
	}

	// End InitializeComponent
	stream_impl << "}" << std::endl << std::endl << std::endl;


	// Paint
	stream_impl << "void " << class_name << "::Paint( dx::Window *sender, dx::BasePainter *painter )" << std::endl; 
	stream_impl << "{" << std::endl;

	stream_impl << std::tab << "painter->PaintRect( { { 0, 0 }, { sender->Width( ), sender->Height( ) } }, dx::Pen( 0xFF222222, 1 ) );" << std::endl;
	generate_function_with_calls( "Paint", "sender, painter", stream_impl );

	// End Paint
	stream_impl << "}" << std::endl << std::endl << std::endl;


	// KeyDown
	stream_impl << "void " << class_name << "::KeyDown( dx::Window *sender, dx::KeyDownArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "KeyDown", "sender, args", stream_impl );

	// End KeyDown
	stream_impl << "}" << std::endl << std::endl << std::endl;


	// KeyUp
	stream_impl << "void " << class_name << "::KeyUp( dx::Window *sender, dx::KeyUpArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "KeyUp", "sender, args", stream_impl );

	// End KeyUp
	stream_impl << "}" << std::endl << std::endl << std::endl;

	// KeyDownChar
	stream_impl << "void " << class_name << "::KeyDownChar( dx::Window *sender, dx::KeyDownCharArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "KeyDownChar", "sender, args", stream_impl );
	// End KeyDownChar
	stream_impl << "}" << std::endl << std::endl << std::endl;

	// MouseMoved
	stream_impl << "void " << class_name << "::MouseMoved( dx::Window *sender, dx::MouseMovedArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "MouseMoved", "sender, args", stream_impl );
	// End MouseMoved
	stream_impl << "}" << std::endl << std::endl << std::endl;

	// MouseClicked
	stream_impl << "void " << class_name << "::MouseClicked( dx::Window *sender, dx::MouseClickedArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;
	
	generate_function_with_calls( "MouseClicked", "sender, args", stream_impl );
	// End MouseClicked
	stream_impl << "}" << std::endl << std::endl << std::endl;

	// MouseReleased
	stream_impl << "void " << class_name << "::MouseReleased( dx::Window *sender, dx::MouseReleasedArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "MouseReleased", "sender, args", stream_impl );
	// End MouseReleased
	stream_impl << "}" << std::endl << std::endl << std::endl;

	// MouseScrolled
	stream_impl << "void " << class_name << "::MouseScrolled( dx::Window *sender, dx::ScrollArgs &args )" << std::endl; 
	stream_impl << "{" << std::endl;

	generate_function_with_calls( "MouseScrolled", "sender, args", stream_impl );
	// End MouseScrolled
	stream_impl << "}" << std::endl << std::endl << std::endl;

	
	// add the event functions
	for ( auto &ev : editor_->get_custom_events( ) )
	{
		auto compData = (Editor::CompData*)ev.component->getUserdata( );
		stream_impl << "void " << class_name << "::" << compData->name_ << "_" << ev.event_name << "( dx::Component *sender )" << std::endl;
		stream_impl << "{" << std::endl;
		stream_impl << std::tab << "throw std::exception( \"Not implemented\" );" << std::endl;
		stream_impl << "}" << std::endl << std::endl << std::endl;
	}

	dx::MsgBox( "Generated output", "Done", dx::DialogType::IconAsterisk | dx::DialogType::OK ).Show( );
}

dx::String Exporter::get_type_str(dx::uint type)
{
	switch(type)
	{
	case Editor::CompData::Button:
		return "dx::Button";
	case Editor::CompData::Checkbox:
		return "dx::Checkbox";
	case Editor::CompData::RichLabel:
		return "dx::RichLabel";
	case Editor::CompData::Slider:
		return "dx::Slider";
	case Editor::CompData::Tab:
		return "dx::Tab";
	case Editor::CompData::TabControl:
		return "dx::TabControl";
	case Editor::CompData::Textbox:
		return "dx::Textbox";
	}
}

void Exporter::setup_type( dx::Component * components, dx::uint type, std::ostream &out )
{
	switch(type)
	{
	case Editor::CompData::Button:
		return setup_button( components, type, out );
	case Editor::CompData::Checkbox:
		return setup_checkbox( components, type, out );
	case Editor::CompData::RichLabel:
		return setup_richlabel( components, type, out );
	case Editor::CompData::Slider:
		return setup_slider( components, type, out );
	case Editor::CompData::Tab:
		return setup_tab( components, type, out );
	case Editor::CompData::TabControl:
		return setup_tabcontrol( components, type, out );
	case Editor::CompData::Textbox:
		return setup_textbox( components, type, out );
	}
}

void Exporter::setup_button(dx::Component* components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;

	output << std::tab << name << "->setText( \"" << components->getText( ) << "\" );" << std::endl;
	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	auto region = components->determineRegion( );
	output << std::tab << name << "->setLocalRegion( { { " << region.position.x << ", " << region.position.y << " }, { " << region.size.x << ", " << region.size.y << " } } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	if ( has_events( components ) )
		implement_events( components, output );
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_checkbox(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;
	output << std::tab << name << "->setText( \"" << components->getText( ) << "\" );" << std::endl;
	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	auto region = components->determineRegion( );
	output << std::tab << name << "->setLocalRegion( { { " << region.position.x << ", " << region.position.y << " }, { " << region.size.x << ", " << region.size.y << " } } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	if ( has_events( components ) )
		implement_events( components, output );
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_richlabel(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;

	output << std::tab << name << "->appendText( \"" << components->getText( ) << "\" );" << std::endl;
	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	auto region = components->determineRegion( );
	output << std::tab << name << "->setLocalRegion( { { " << region.position.x << ", " << region.position.y << " }, { " << region.size.x << ", " << region.size.y << " } } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	if ( has_events( components ) )
		implement_events( components, output );
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_slider(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;
	auto slider = (dx::Slider*)components;

	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	output << std::tab << name << "->setMaxDelta( " << slider->getMaxDelta( ) << " );" << std::endl;
	output << std::tab << name << "->setWheelSize( { " << slider->getWheelSize( ).x << ", " << slider->getWheelSize( ).y << " } );" << std::endl;
	output << std::tab << name << "->setWheel( " << slider->getWheel( ) << " );" << std::endl;
	output << std::tab << name << "->setDelta( " << slider->getDelta( ).x << " );" << std::endl;
	auto region = components->determineRegion( );
	output << std::tab << name << "->setLocalRegion( { { " << region.position.x << ", " << region.position.y << " }, { " << region.size.x << ", " << region.size.y << " } } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	if ( has_events( components ) )
		implement_events( components, output );
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_tab(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata();
	auto name = compData->name_;
	auto tabctrl = (dx::TabControl*)components->getParent( );
	auto tabData = (Editor::CompData*)tabctrl->getUserdata( );

	output << std::tab << name << "->setText( \"" << components->getText( ) << "\" );" << std::endl;
	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	output << std::tab << name << "->setSize( { " << components->getSize( ).x << ", " << components->getSize( ).y << " } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	output << std::tab << tabData->name_ << "->addTab( (dx::Tab*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_tabcontrol(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;

	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	output << std::tab << name << "->setLocalPosition( { " << components->getLocalPosition( ).x << ", " << components->getLocalPosition( ).y << " } );" << std::endl;
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::setup_textbox(dx::Component * components, dx::uint type, std::ostream & output)
{
	auto compData = (Editor::CompData*)components->getUserdata( );
	auto name = compData->name_;

	output << std::tab << name << "->setText( \"" << components->getText( ) << "\" );" << std::endl;
	output << std::tab << name << "->setUIID( " << components->getUIID( ) << " );" << std::endl;
	auto region = components->determineRegion( );
	output << std::tab << name << "->setLocalRegion( { { " << region.position.x << ", " << region.position.y << " }, { " << region.size.x << ", " << region.size.y << " } } );" << std::endl;
	output << std::tab << name << "->setStyle( dx::StyleManager( dx::Theme::Dark, dx::Style::Blue ) );" << std::endl;
	if ( has_events( components ) )
		implement_events( components, output );
	if ( compData->tab_ )
		output << std::tab << ((Editor::CompData*)compData->tab_->getUserdata( ))->name_ << "->addComponent( (dx::Component*)" << name << " );" << std::endl;
	output << std::endl;
}

void Exporter::generate_function_with_calls(dx::String function_to_call, dx::String args, std::ostream &out)
{
	auto added = editor_->get_added( );
	for ( auto it = added.begin( ), end = added.end( ); it < end; ++it )
	{
		auto &x = *it;
		auto compData = (Editor::CompData*)x->getUserdata( );

		if ( compData->tab_ )
			continue;

		if ( compData->type == compData->Tab )
			continue;
		
		out << std::tab << compData->name_ << "->" << function_to_call << "( " << args << " );" << std::endl;
	}
}

bool Exporter::has_events(dx::Component * component)
{
	for ( auto &x : editor_->get_custom_events( ) )
		if ( x.component == component )
			return true;
	return false;
}

void Exporter::implement_events(dx::Component * component, std::ostream & out)
{
	auto compData = (Editor::CompData*)component->getUserdata( );
	std::vector<Editor::EventWrapper> events;
	for ( auto &x : editor_->get_custom_events( ) )
		if ( x.component == component )
			events.emplace_back( x );

	for ( auto &x : events )
		out << std::tab << compData->name_ << "->" << x.event_name << "( ) += [this]( dx::Component *sender ) { this->" << compData->name_ << "_" << x.event_name << "( sender ); };" << std::endl;
}
